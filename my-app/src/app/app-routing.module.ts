import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {ChartsComponent} from './charts/charts.component';


const routes: Routes = [
  { path: '', redirectTo: 'crud', pathMatch: 'full'},
  { path: 'crud', loadChildren: './crud/crud.module#CrudModule'},
  {path: 'login', component:LoginComponent},
  {path: 'bar-chart', component:ChartsComponent},
  {path: '**', redirectTo: 'crud'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
