import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Employee} from '../entity/employee';
import { Observable,of, from  } from 'rxjs';
import {User} from '../entity/user';


@Injectable({
  providedIn: 'root'
})

@Injectable()
export class EmployeeService {
  private employeeUrl ='http://localhost:8081/employee';
  private employeesListUrl = 'http://localhost:8081/listemployee';
  private employeeLogin = 'http://localhost:8081/login';
  private employeesListUrlByIdc = 'http://localhost:8081/company';
  private token = "Bearer "+localStorage.getItem("token");

  private httpAuthorization = {
  headers : new HttpHeaders({'Content-Type': 'application/json'}).append("Authorization",this.token)
  };

  constructor(private http: HttpClient) {

  }

  login(user: User): Observable<any> {
    return this.http.post(`${this.employeeLogin}`, user);
  }

  getEmployee(id: number): Observable<any> {
    return this.http.get(`${this.employeeUrl}?idE=${id}`);
  }

  createEmployee(employee: Employee): Observable<Object> {
    this.resetToken();
    console.log(this.httpAuthorization);
    return this.http.post(`${this.employeeUrl}`, employee, this.httpAuthorization);
  }


  updateEmployee(id: number, employee: Employee): Observable<Object> {
    this.resetToken();
    console.log(this.httpAuthorization);
    return this.http.put(`${this.employeeUrl}?idE=${id}`, employee, this.httpAuthorization);
  }

  deleteEmployee(id: number): Observable<any> {
    this.resetToken();
    console.log(this.httpAuthorization);
    return this.http.delete(`${this.employeeUrl}?idE=${id}`, this.httpAuthorization);
  }

  getEmployeesList(): Observable<any> {
    return this.http.get(`${this.employeesListUrl}`);
  }

  getEmployeesListByIdc(id: number): Observable<any> {
    return this.http.get(`${this.employeesListUrlByIdc}idC=${id}`);
  }

  resetToken(): void {
    this.token = "Bearer "+localStorage.getItem("token");
    this.httpAuthorization = {
      headers : new HttpHeaders({'Content-Type': 'application/json'}).append("Authorization",this.token)
    };
  }
}
