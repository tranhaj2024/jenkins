import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable,of, from  } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  private companysURL ='http://localhost:8081/';
  private amountcompanysURL = 'http://localhost:8081/amount';
  private token = "Bearer "+localStorage.getItem("token");

  private httpAuthorization = {
    headers : new HttpHeaders({'Content-Type': 'application/json'}).append("Authorization",this.token)
  };


  constructor(private http: HttpClient ) { }

  getCompanyList(): Observable<any> {
    return this.http.get(`${this.companysURL}`);
  }

  getAmoutCompany(): Observable<any> {
    return this.http.get(`${this.amountcompanysURL}`);
  }
}
