import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import {Observable} from 'rxjs';
import {EmployeeService} from '../service/employee.service';
import {Router} from '@angular/router';
import {CompanyService} from '../service/company.service';
import {AmountCompany} from '../entity/amountcompany';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas'

@Component({
  selector: 'app-bar-chart',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.scss'],
  providers: [EmployeeService, CompanyService]
})
export class ChartsComponent implements OnInit {

  amountCompanyList : Observable<AmountCompany[]>;

  public barChartOptions: ChartOptions = {
    // scaleShowVerticalLines: false,
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels =  [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  public barChartData: ChartDataSets[] = [
    { data : [0], label: 'Số lượng nhân viên' }
  ];

  constructor(private employeeService: EmployeeService, private companyService: CompanyService, private router: Router) { }

  loadAmountCompany() {
    this.amountCompanyList = this.companyService.getAmoutCompany();
  }


  ngOnInit() {
    this.loadAmountCompany();
    this.amountCompanyList.subscribe(data => {
        console.log(data);
        data.forEach(amountCompany => {
          this.barChartLabels.push(amountCompany.name);
          this.barChartData[0].data.unshift(amountCompany.amount);
        })
    },
      error => {
         console.log(error)
      });
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public randomize(): void {
    // Only Change 3 values
    const data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    this.barChartData[0].data = data;
  }

  @ViewChild('content') content:ElementRef;
  SavePDF() {
    let content=this.content.nativeElement;
    let doc = new jsPDF();
    let _elementHandlers =
      {
        '#editor':function(element,renderer){
          return true;
        }
      };
    doc.fromHTML(content.innerHTML,15,125,{

      'width':290,
      'elementHandlers':_elementHandlers
    });

    doc.save('test.pdf');
  }


  @ViewChild('screen') screen: ElementRef;
  @ViewChild('canvas') canvas: ElementRef;
  @ViewChild('downloadLink') downloadLink: ElementRef;
  downloadImage(){
    html2canvas(this.screen.nativeElement).then(canvas => {
      this.canvas.nativeElement.src = canvas.toDataURL();
      this.downloadLink.nativeElement.href = canvas.toDataURL('image/png');
      this.downloadLink.nativeElement.download = 'marble-diagram.png';
      this.downloadLink.nativeElement.click();
    });
  }

}
