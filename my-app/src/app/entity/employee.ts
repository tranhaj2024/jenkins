import {Company} from './company';

export class Employee {
  idE: String;
  nameE: String;
  email: String;
  phone: String;
  birthday: String;
  company : Company;
  passworde: String;
}
