import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {EmployeeListComponent} from './employee-list/employee-list.component';
import {CreateEmployeeComponent} from './create-employee/create-employee.component';
import {UpdateEmployeeComponent} from './update-employee/update-employee.component';
import {CrudComponent} from './crud.component';


const routes : Routes = [
  {
    path: '', component: CrudComponent,
    children: [
      {path: '', redirectTo: 'employees', pathMatch: 'full'},
      {path: 'employees', component: EmployeeListComponent},
      {path: 'add', component: CreateEmployeeComponent},
      {path: 'update/:id', component: UpdateEmployeeComponent},
    ]
  }
];


@NgModule({

  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})

export class CrudRoutingModule {
}

