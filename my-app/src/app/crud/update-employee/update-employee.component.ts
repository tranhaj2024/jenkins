import { Component, OnInit } from '@angular/core';
import {Employee} from '../../entity/employee';

import {ActivatedRoute, Router} from '@angular/router';
import {EmployeeService} from '../../service/employee.service';
import {Company} from '../../entity/company';

@Component({
  selector: 'app-update-employee',
  templateUrl: './update-employee.component.html',
  // styleUrls: ['']
})
export class UpdateEmployeeComponent implements OnInit {

  idE : number;
  employee: Employee;
  company: Company;

  constructor(private route: ActivatedRoute,private router: Router,
              private employeeService: EmployeeService) {
  }

  ngOnInit(): void {
    this.employee = new Employee();

    this.idE = this.route.snapshot.params['id'];

    this.employeeService.getEmployee(this.idE)
      .subscribe(data => {
        console.log(data)
        this.employee = data;
        this.company = this.employee.company;
      }, error => console.log(error));
  }

  updateEmployee() {
    this.employeeService.updateEmployee(this.idE, this.employee)
      .subscribe(data => {
        console.log(data)
        if(data) {
          alert("Sửa thành công");
        } else {
          alert("Sửa thất bại");
        }
      },
          error =>  {
            console.log(error);
            alert("Sửa thất bại");
          });
    this.reload();
  }

  onSubmit() {
    this.employee.company.idC = this.company.idC;
    this.updateEmployee();
  }

  reload() {
    this.router.navigate(['/update/'+this.employee.idE]);
  }

}
