import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.css']
})
export class CrudComponent implements OnInit {

  private location: Location;

  constructor(private router: Router) { }

  goBack(): void {
    this.location.back();
  }


  ngOnInit(): void {
  }

}
