import { Component, OnInit } from '@angular/core';
import {Employee} from '../../entity/employee';
import {ActivatedRoute, Router} from '@angular/router';
import {Company} from '../../entity/company';
import {EmployeeService} from '../../service/employee.service';
import { FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';


@Component({
  selector: 'app-employee-create',
  templateUrl: './create-employee.component.html',
  // styleUrls: ['styles.css']
})
export class CreateEmployeeComponent implements OnInit {
  validForm: FormGroup;
  employee: Employee = new Employee();
  companyHere: Company = new Company();
  submitted = false;

  constructor(private router: Router,
              private employeeService: EmployeeService) {
  }

  newEmployee(): void {
    this.submitted = false;
    this.employee = new Employee();
  }

  save(value) {
    this.companyHere.idC = value.idC
    this.employee.company = this.companyHere;
    this.employee.email = value.email;
    this.employee.phone = value.phone;
    this.employee.nameE = value.name;
    this.employee.birthday = value.birthday;
    this.employee.passworde =value.passworde;
    console.log(this.employee);
    this.employeeService.createEmployee(this.employee).subscribe(data => {
      console.log(data);
      if (data ) {
        alert("Thêm " + this.employee.nameE + " thành công")
      } else {
        alert("Server gặp lỗi. Thêm thất bại")
      }
      this.newEmployee();
      this.reload();
    },
        error => {
          console.log(error),
          alert("Bạn không đủ quyền hoặc trùng email  ");
          this.newEmployee();
          this.reload();
        });

  }

  onSubmit(value) {
    console.log(value);
      if (localStorage.getItem("token") != null) {
        this.submitted = true;
        this.save(value);
      } else {
        alert("Bạn cần đăng nhập trước khi thực hiện thao tác");
        this.router.navigate(['/login']);
      }
  }

  reload() {
    this.router.navigate(['/add'])
  }

  ngOnInit() {
    this.validForm = new FormGroup({
      'email' : new FormControl(this.employee.email, [
          Validators.email,
      ]),
      'phone' : new FormControl(this.employee.phone, [
        Validators.pattern("(09|03|01|05|08)+([0-9]{7,10})\\b")
      ]),
      'passworde' : new FormControl(this.employee.passworde, [
        Validators.pattern("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{6,}$")
      ]),
      'confirmPassword' : new FormControl(this.employee.passworde, [
        Validators.pattern("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{6,}$")
      ]),
      'birthday': new FormControl(this.employee.birthday),
      'name': new FormControl(this.employee.nameE),
      'idC': new FormControl(),
    });
  }

  get email() {
      return this.validForm.get('email');
  }

  get phone() {
    return this.validForm.get('phone');
  }

  get password() {
    return this.validForm.get('passworde');
  }

  get confirmPassword() {
    return this.validForm.get("confirmPassword");
  }

}
