import { Component, OnInit } from '@angular/core';
import {Employee} from '../../entity/employee';
import {EmployeeService} from '../../service/employee.service';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  // styleUrls: [''],
  providers: [EmployeeService]
})
export class EmployeeListComponent implements OnInit {

  employees: Observable<Employee[]>;
  employeesfound: Employee[] = [];
  keyword : string;

  constructor(private employeeService: EmployeeService, private router: Router) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.employees = this.employeeService.getEmployeesList();
  }

  deleteEmployee(idE: number) {
    if(localStorage.getItem("token") != null) {
      if (confirm("Bạn có chắc muốn xóa ?")) {
        this.employeeService.deleteEmployee(idE)
          .subscribe(
            data => {
              console.log(data);
              if (data) {
                alert("Xóa thành công ");
              } else {
                alert("gặp lỗi ");
              }
              this.reloadData();
            },
            error => {
          console.log(error),
            alert("Bạn không đủ quyền");
        });
      }
    } else {
      alert("Bạn cần đăng nhập trước khi thực hiện thao tác")
      this.router.navigate(['/login']);
    }
  }


  updateEmployee(id: number){
    if(localStorage.getItem("token") != null) {
      this.router.navigate(['/update/' + id]);
    } else {
      alert("Bạn cần đăng nhập trước khi thực hiện thao tác")
      this.router.navigate(['/login']);
    }
  }

  onSearch() {
    this.employeesfound = [];
    this.employees.forEach(data => {
      data.forEach( employeee => {
        if(employeee.nameE.search(this.keyword) != -1) {
          this.employeesfound.push(employeee);
        }
      })
    });
    console.log(this.employeesfound);
  }
}
