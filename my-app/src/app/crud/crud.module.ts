import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EmployeeListComponent} from './employee-list/employee-list.component';
import {CreateEmployeeComponent} from './create-employee/create-employee.component';
import {UpdateEmployeeComponent} from './update-employee/update-employee.component';
import {EmployeeService} from '../service/employee.service';
import {CrudRoutingModule} from './crud-routing.module';
import {CrudComponent} from './crud.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {CompanyService} from '../service/company.service';


@NgModule({
  declarations: [
    EmployeeListComponent,
    CreateEmployeeComponent,
    UpdateEmployeeComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    CrudRoutingModule,
    ReactiveFormsModule
  ],
  providers: [
    EmployeeService,
    CompanyService
  ],
  bootstrap: [CrudComponent]
})
export class CrudModule { }
