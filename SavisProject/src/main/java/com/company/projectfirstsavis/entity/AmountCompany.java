package com.company.projectfirstsavis.entity;

import java.io.Serializable;

public class AmountCompany  implements Serializable {
    private static final long serialVersionUID = -8091879091924046844L;
    private int idC;
    private int amount;
    private String name;

    public AmountCompany ( int idC, int amount, String name ) {
        this.idC = idC;
        this.amount = amount;
        this.name = name;
    }

    public int getIdC () {
        return idC;
    }

    public void setIdC ( int idC ) {
        this.idC = idC;
    }

    public int getAmount () {
        return amount;
    }

    public void setAmount ( int amount ) {
        this.amount = amount;
    }

    public String getName () {
        return name;
    }

    public void setName ( String name ) {
        this.name = name;
    }
}
