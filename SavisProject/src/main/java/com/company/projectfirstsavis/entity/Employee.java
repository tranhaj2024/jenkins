package com.company.projectfirstsavis.entity;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;


@Entity
@Table(name = "employee")
public class Employee implements Serializable {
    private static final long serialVersionUID = 5926468583005150707L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ide;

    @Column(name = "namee")
    @Size(max = 30)
    private String nameE;

    @Column(name = "email", unique = true)
    @Email
    @Size(min = 5, max = 50, message = "{casa.nomcaac.size}")
    private String email;

    @Column(name = "phone", unique = true)
    @Size(min = 5, max = 20, message = "{casa.nomcaac.size}")
    @Pattern(regexp = "(09|03|01|05|08)+([0-9]{7,10})\\b")
    private String phone;

    @Column(name = "birthday")
    private String birthday;

    @ManyToOne
    @JoinColumn(name = "idC")
    private Company company;

    @Column(name = "passworde")
    @Size(min = 6, message = "Password size not valid")
    private String passworde;

    @Column(name="user_role")
    @Size(max = 20)
    private String user_role;

    @Column(name = "password_salt")
    @Size(min = 6)
    private String password_salt;

    public Employee () {
    }

    public Employee ( String nameE, String email, String phone, String birthday, Company company, String passworde, String user_role, String password_salt ) {
        this.nameE = nameE;
        this.email = email;
        this.phone = phone;
        this.birthday = birthday;
        this.company = company;
        this.passworde = passworde;
        this.user_role = user_role;
        this.password_salt = password_salt;
    }

    public String getPassword_salt () {
        return password_salt;
    }

    public void setPassword_salt ( String password_salt ) {
        this.password_salt = password_salt;
    }

    public String getPassworde () {
        return passworde;
    }

    public void setPassworde ( String passworde ) {
        this.passworde = passworde;
    }

    public String getUser_role () {
        return user_role;
    }

    public void setUser_role ( String user_role ) {
        this.user_role = user_role;
    }

    public Integer getIdE () {
        return ide;
    }

    public void setIdE ( Integer idE ) {
        this.ide = idE;
    }

    public String getNameE () {
        return nameE;
    }

    public void setNameE ( String nameE ) {
        this.nameE = nameE;
    }

    public String getEmail () {
        return email;
    }

    public void setEmail ( String email ) {
        this.email = email;
    }

    public String getPhone () {
        return phone;
    }

    public void setPhone ( String phone ) {
        this.phone = phone;
    }

    public String getBirthday () {
        return birthday;
    }

    public void setBirthday ( String birthday ) {
        this.birthday = birthday;
    }

    public Company getCompany () {
        return company;
    }

    public void setCompany ( Company company ) {
        this.company = company;
    }

    public String toString() {
        return  this.nameE;
    }
}
