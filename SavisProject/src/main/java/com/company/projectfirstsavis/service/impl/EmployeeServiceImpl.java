package com.company.projectfirstsavis.service.impl;

import com.company.projectfirstsavis.entity.Employee;
import com.company.projectfirstsavis.repository.EmployeeRepository;
import com.company.projectfirstsavis.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public List<Employee> getAllEmployee () {
        List<Employee> e = (List<Employee>) employeeRepository.findAll();
        return e;
    }

    @Override
    public void saveEmployee ( Employee employee ) {
        employeeRepository.save(employee);
    }

    @Override
    public void deleteEmployee ( Integer idE ) {
        employeeRepository.deleteById(idE);
    }

    @Override
    public Optional<Employee> findUserById ( Integer idE ) {
        return employeeRepository.findById(idE);
    }

    @Override
    public Employee loadEmployeeFromEmail ( String email ) {

        return employeeRepository.loadEmployeeFromEmail(email);
    }

    @Override
    public Employee authenticateEmployee ( String email, String password ) {
        return employeeRepository.authenticateEmployee(email, password);
    }

    @Override
    public String getSaltFromEmail ( String email ) {
        return employeeRepository.getSaltFromEmail(email);
    }

    @Override
    public List<Employee> findByIdcCompany ( Integer idc ) {
        return employeeRepository.findByIdcCompany(idc);
    }

}
