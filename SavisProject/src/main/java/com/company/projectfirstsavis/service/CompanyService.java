package com.company.projectfirstsavis.service;

import com.company.projectfirstsavis.entity.Company;
import com.company.projectfirstsavis.entity.Employee;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface CompanyService {
    public Optional<Company> getCompany ( Integer idC);

    public List<Company> getCompanys();

    public Integer countAllByIdc( Integer idc);
}
