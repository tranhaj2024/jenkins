package com.company.projectfirstsavis.service.impl;

import com.company.projectfirstsavis.entity.Company;
import com.company.projectfirstsavis.entity.Employee;
import com.company.projectfirstsavis.repository.CompanyRepository;
import com.company.projectfirstsavis.repository.EmployeeRepository;
import com.company.projectfirstsavis.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Optional<Company> getCompany ( Integer idC) {
        return companyRepository.findById(idC);
    }

    @Override
    public List<Company> getCompanys () {
        return companyRepository.findAll();
    }

    @Override
    public Integer countAllByIdc ( Integer idc ) {
        return companyRepository.countAllByIdc(idc);
    }


}
