package com.company.projectfirstsavis.service;

import com.company.projectfirstsavis.entity.Employee;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {

    public List<Employee> getAllEmployee();

    public void saveEmployee(Employee employee);

    public void deleteEmployee(Integer idE);

    Optional<Employee> findUserById(Integer idE);

    public Employee loadEmployeeFromEmail(String email);

    public Employee authenticateEmployee(String email, String password);

    public String getSaltFromEmail(String email);

    public List<Employee> findByIdcCompany ( Integer idc );
}
