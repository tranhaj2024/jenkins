package com.company.projectfirstsavis.service.impl;

import com.company.projectfirstsavis.entity.Employee;
import com.company.projectfirstsavis.repository.EmployeeRepository;
import com.sun.security.auth.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;

@Service

public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private EmployeeRepository usersRepository;

//    @Bean
//    public UserDetailsService getUserDetails(){
//        return new UserDetailsServiceImpl(); // Implementation class
//    }

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername( String login) throws UsernameNotFoundException {
        try {
            Employee employee = usersRepository.loadEmployeeFromEmail(login);
            if (employee == null) {
                throw new UsernameNotFoundException("User not found");
            }
            Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
            grantedAuthorities.add(new SimpleGrantedAuthority(employee.getUser_role()));
            return new org.springframework.security.core.userdetails.User(
                    employee.getEmail(), "$2a$16$AYSTH/JEPXPwqYBRswLq0emwmItvsQgf.dnaffXGJpvYML97bBoGe", grantedAuthorities);
        } catch (NoSuchElementException e) {
            throw new UsernameNotFoundException("User " + login + " not found.", e);
        }
    }

}