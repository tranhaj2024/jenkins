package com.company.projectfirstsavis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectfirstsavisApplication {

    public static void main ( String[] args ) {
        SpringApplication.run(ProjectfirstsavisApplication.class, args);
    }

}
