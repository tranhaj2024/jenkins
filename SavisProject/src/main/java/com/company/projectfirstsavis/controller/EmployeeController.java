package com.company.projectfirstsavis.controller;

import com.company.projectfirstsavis.entity.Employee;
import com.company.projectfirstsavis.security.JBcryptHelper;
import com.company.projectfirstsavis.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

//Responsebody + Controller
@RestController
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private JBcryptHelper jBcryptHelper;

    //RequestMapping+ method(GET)
    @GetMapping(value = "/employee")
    public ResponseEntity<Optional<Employee>> getAllEmployee(@RequestParam Integer idE) {
        try {
            Optional<Employee> employee = employeeService.findUserById(idE);
            System.out.println("Đây là mới ");
            return new ResponseEntity<>(employee, HttpStatus.OK);
        }catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @PostMapping(value = "/employee")
    public ResponseEntity<?> addEmployee(@Valid @RequestBody  Employee employee ) {
        try {
                String totalPassword[] = jBcryptHelper.generatePasswordByHashPlantextAndSalt(employee.getPassworde());
                employee.setPassworde(totalPassword[0]);
                employee.setPassword_salt(totalPassword[1]);
                employeeService.saveEmployee(employee);
                return new ResponseEntity<>(true, HttpStatus.OK);
        }catch (Exception ex ) {
            System.err.println("Error in post employee Controller");
            return new ResponseEntity<>(false, HttpStatus.CONFLICT);
        }
    }

    @DeleteMapping(value = "/employee")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Boolean> deleteEmployee(@RequestParam Integer idE) {
        try {
            employeeService.deleteEmployee(idE);
            return new ResponseEntity<>(true, HttpStatus.OK);
        }catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(false, HttpStatus.CONFLICT);
        }
    }

    @PutMapping(value = "/employee")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Boolean> updateEmployee( @RequestParam Integer idE,@Valid @RequestBody  Employee employee) {
        try {
            Optional<Employee> currentEmployee = employeeService.findUserById(idE);
            if ((!currentEmployee.isPresent())) {
                System.out.println("Không tìm được nhân viên cần sửa");
                return new ResponseEntity<>(false, HttpStatus.CONFLICT);
            }
            employee.setIdE(idE);
            employee.setPassworde(currentEmployee.get().getPassworde());

            employeeService.saveEmployee(employee);
            return new ResponseEntity<>(true, HttpStatus.OK);
        }catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(false, HttpStatus.CONFLICT);
        }
    }
}
