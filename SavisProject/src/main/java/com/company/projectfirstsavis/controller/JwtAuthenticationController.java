package com.company.projectfirstsavis.controller;

import com.company.projectfirstsavis.security.JBcryptHelper;
import com.company.projectfirstsavis.service.impl.JwtTokenUtil;
import com.company.projectfirstsavis.entity.Employee;
import com.company.projectfirstsavis.entity.JwtRequest;
import com.company.projectfirstsavis.entity.JwtResponse;
import com.company.projectfirstsavis.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class JwtAuthenticationController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private JBcryptHelper jBcryptHelper;


    @PostMapping(value = "/login")
    public ResponseEntity<?> createAuthenticationToken( @RequestBody JwtRequest authenticationRequest) throws Exception {
        try {
            //authenticate(authenticationRequest.getEmail(), authenticationRequest.getPassword());
            String email = authenticationRequest.getEmail();
            String planTextPassword = authenticationRequest.getPassword();

            //Lấy employee từ email
            Employee employee = employeeService.loadEmployeeFromEmail(email);
            //Công chuỗi plantext và hash
            String hashPassword = planTextPassword + employee.getPassword_salt();
            if (jBcryptHelper.checkBcryptPassword(hashPassword, employee.getPassworde())) {
                final String token = jwtTokenUtil.generateToken(employee);
                return ResponseEntity.ok(new JwtResponse(token));
            }
            return new ResponseEntity<>(true, HttpStatus.NOT_FOUND);
        }catch (Exception ex) {
            ex.printStackTrace();
            System.err.println("Error in createAuthenticationToken Controller");
            return new ResponseEntity<>(true, HttpStatus.CONFLICT);
        }
    }

    private void authenticate(String email, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
