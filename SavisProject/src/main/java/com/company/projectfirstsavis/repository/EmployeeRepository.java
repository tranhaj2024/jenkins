package com.company.projectfirstsavis.repository;


import com.company.projectfirstsavis.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    @Query(value = "select * from Employee e where e.email = ?1", nativeQuery = true)
    public Employee loadEmployeeFromEmail(String email);

    @Query(value = "select * from Employee e where e.email = ?1 and e.passworde = ?2", nativeQuery = true)
    public Employee authenticateEmployee(String email, String password);

    @Query(value = "select password_salt from Employee e where e.email = ?1", nativeQuery = true)
    public String getSaltFromEmail(String email);

    @Query(value = "SELECT * FROM dbo.Employee WHERE idc = ?1", nativeQuery = true)
    public List<Employee> findByIdcCompany( Integer idc);


}
