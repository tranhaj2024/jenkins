package com.company.projectfirstsavis.repository;

import com.company.projectfirstsavis.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Integer> {
    @Query(value = "SELECT count(ide) FROM dbo.Employee WHERE idc = ?1", nativeQuery = true)
    public Integer countAllByIdc( Integer idc);
}
